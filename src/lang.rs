use super::grammar;
use std::collections::HashMap;

pub fn get_language_grammar() -> grammar::Grammar {
    let mut grammar = grammar::Grammar {
        rules: HashMap::new(),
        start_rule: "grammar".to_string(),
    };

    // grammar: statement+
    grammar.rules.insert(
        "grammar".to_string(),
        Box::new(grammar::complex::OneOrMore {
            expression: Box::new(grammar::atomic::AtomicNonTerminal {
                name: "statement".to_string(),
            }),
        }),
    );

    // identifier: SOME-CHARS+
    grammar.rules.insert(
        "identifier".to_string(),
        Box::new(grammar::complex::OneOrMore {
            expression: Box::new(grammar::atomic::AtomicChars {
                name: "IDENTIFIER".to_string(),
                valid_chars: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"
                    .to_string(),
            }),
        }),
    );

    // non-newline-whitespace: space or tab
    grammar.rules.insert(
        "non-newline-whitespace".to_string(),
        Box::new(grammar::complex::OneOrMore {
            expression: Box::new(grammar::atomic::AtomicChars {
                name: "WHITESPACE".to_string(),
                valid_chars: " \t".to_string(),
            }),
        }),
    );
    // any whitespace!
    grammar.rules.insert(
        "any-whitespace".to_string(),
        Box::new(grammar::complex::OneOrMore {
            expression: Box::new(grammar::atomic::AtomicChars {
                name: "ANY_WHITESPACE".to_string(),
                valid_chars: " \t\n\r".to_string(),
            }),
        }),
    );

    // statement: line | comment | newline
    grammar.rules.insert(
        "statement".to_string(),
        Box::new(grammar::complex::OrderedChoice {
            options: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "line".to_string(),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "comment".to_string(),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "newline".to_string(),
                }),
            ],
        }),
    );

    // comment: '#' (characters | whitespace)* newline
    grammar.rules.insert(
        "comment".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicTerminal { value: '#' }),
                Box::new(grammar::complex::OneOrMore {
                    expression: Box::new(grammar::atomic::AtomicAnythingExcept { not_this: '\n' }),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "newline".to_string(),
                }),
            ],
        }),
    );

    // line: non-terminal ':' sequence-expression newline
    grammar.rules.insert(
        "line".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "non-terminal".to_string(),
                }),
                Box::new(grammar::atomic::AtomicTerminal { value: ':' }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "non-newline-whitespace".to_string(),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "sequence-expression".to_string(),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "newline".to_string(),
                }),
            ],
        }),
    );

    // newline: \n
    grammar.rules.insert(
        "newline".to_string(),
        Box::new(grammar::atomic::AtomicTerminal { value: '\n' }),
    );

    // non-terminal: ALPHA+
    grammar.rules.insert(
        "non-terminal".to_string(),
        Box::new(grammar::atomic::AtomicNonTerminal {
            name: "identifier".to_string(),
        }),
    );

    // sequence-expression: ordered-choice-expression (' ' ordered-choice-expression)*
    grammar.rules.insert(
        "sequence-expression".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "ordered-choice-expression".to_string(),
                }),
                Box::new(grammar::complex::ZeroOrMore {
                    expression: Box::new(grammar::complex::Sequence {
                        expressions: vec![
                            Box::new(grammar::atomic::AtomicTerminal { value: ' ' }),
                            Box::new(grammar::atomic::AtomicNonTerminal {
                                name: "ordered-choice-expression".to_string(),
                            }),
                        ],
                    }),
                }),
            ],
        }),
    );

    // ordered-choice-expression: value-expression ('|' value-expression)(for now)
    grammar.rules.insert(
        "ordered-choice-expression".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "zero-or-more-expression".to_string(),
                }),
                Box::new(grammar::complex::ZeroOrMore {
                    expression: Box::new(grammar::complex::Sequence {
                        expressions: vec![
                            Box::new(grammar::atomic::AtomicNonTerminal {
                                name: "non-newline-whitespace".to_string(),
                            }),
                            Box::new(grammar::atomic::AtomicTerminal { value: '|' }),
                            Box::new(grammar::atomic::AtomicNonTerminal {
                                name: "non-newline-whitespace".to_string(),
                            }),
                            Box::new(grammar::atomic::AtomicNonTerminal {
                                name: "zero-or-more-expression".to_string(),
                            }),
                        ],
                    }),
                }),
            ],
        }),
    );

    grammar.rules.insert(
        "zero-or-more-expression".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "one-or-more-expression".to_string(),
                }),
                Box::new(grammar::complex::Optional {
                    expression: Box::new(grammar::atomic::AtomicTerminal { value: '*' }),
                }),
            ],
        }),
    );

    grammar.rules.insert(
        "one-or-more-expression".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "optional-expression".to_string(),
                }),
                Box::new(grammar::complex::Optional {
                    expression: Box::new(grammar::atomic::AtomicTerminal { value: '+' }),
                }),
            ],
        }),
    );

    grammar.rules.insert(
        "optional-expression".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "value-expression".to_string(),
                }),
                Box::new(grammar::complex::Optional {
                    expression: Box::new(grammar::atomic::AtomicTerminal { value: '?' }),
                }),
            ],
        }),
    );

    grammar.rules.insert(
        "value-expression".to_string(),
        Box::new(grammar::complex::OrderedChoice {
            options: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "atomic-expression".to_string(),
                }),
                Box::new(grammar::complex::Sequence {
                    expressions: vec![
                        Box::new(grammar::atomic::AtomicTerminal { value: '(' }),
                        Box::new(grammar::atomic::AtomicNonTerminal {
                            name: "sequence-expression".to_string(),
                        }),
                        Box::new(grammar::atomic::AtomicTerminal { value: ')' }),
                    ],
                }),
            ],
        }),
    );

    // atomic-expression: terminal | non-terminal | empty
    grammar.rules.insert(
        "atomic-expression".to_string(),
        Box::new(grammar::complex::OrderedChoice {
            options: vec![
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "terminal".to_string(),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "non-terminal".to_string(),
                }),
                Box::new(grammar::atomic::AtomicNonTerminal {
                    name: "string".to_string(),
                }),
                //Box::new(grammar::atomic::AtomicEmpty{}),
            ],
        }),
    );

    // terminal: 'CHAR'
    grammar.rules.insert(
        "terminal".to_string(),
        Box::new(grammar::complex::OrderedChoice {
            options: vec![Box::new(grammar::complex::Sequence {
                expressions: vec![
                    Box::new(grammar::atomic::AtomicTerminal { value: '\'' }),
                    Box::new(grammar::atomic::AtomicAnythingExcept { not_this: '\n' }),
                    Box::new(grammar::atomic::AtomicTerminal { value: '\'' }),
                ],
            })],
        }),
    );
    // string: "stuff"
    grammar.rules.insert(
        "string".to_string(),
        Box::new(grammar::complex::Sequence {
            expressions: vec![
                Box::new(grammar::atomic::AtomicTerminal { value: '"' }),
                Box::new(grammar::complex::OneOrMore {
                    expression: Box::new(grammar::atomic::AtomicAnythingExcept { not_this: '"' }),
                }),
                Box::new(grammar::atomic::AtomicTerminal { value: '"' }),
            ],
        }),
    );

    grammar
}
