use super::lang;
use super::syntax::Node;
use std::collections::{HashMap, HashSet};
use std::fmt;

pub mod atomic;
pub mod complex;

#[derive(Debug)]
pub enum ParseResult {
    Fail(String),
    Success(Node, usize),
    EmptySuccess,
}

pub struct ParseState<'a> {
    input: &'a str,
    index: usize,
    depth: usize,
    non_terminals: HashSet<String>,
    grammar: &'a Grammar,
}

pub trait Expression {
    fn parse(&self, _state: ParseState) -> ParseResult {
        // fn parse(&self, input: &String, index: usize, depth: usize, grammar: &Grammar) -> ParseResult {
        // Take input string, index of current position, depth of recursion, non-terminals that we've already seen
        // and the grammar that we're parsing with
        // Return the parse result
        panic!("IMPLEMENT ME!!");
    }

    fn self_to_string(&self) -> String {
        "NOT IMPLEMENTED".to_string()
    }
}

impl std::fmt::Debug for dyn Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self.self_to_string())
    }
}

impl fmt::Display for dyn Expression {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.self_to_string())
    }
}

#[derive(Debug)]
pub struct Grammar {
    pub rules: HashMap<String, Box<dyn Expression>>,
    pub start_rule: String,
}

impl Grammar {
    pub fn load_from_string(content: &str) -> Result<Grammar, String> {
        let lang_gram = lang::get_language_grammar();
        let result = lang_gram.parse(content.to_string());
        match result {
            None => {
                println!("Failed...");
                Err("Failed to parse.".to_string())
            }
            Some(node) => {
                println!("\n\n-------------------");
                println!("--- get grammar ---");
                println!("-------------------\n");
                println!("{}", lang_gram);
                println!("(load) PARSE TREE:\n{}", node);

                match Node::get_grammar(node) {
                    Some(grammar) => {
                        println!("{}", grammar);
                        Ok(grammar)
                    }
                    None => Err("Nothing was parsed.".to_string()),
                }
            }
        }
    }

    // FIXME: convert Option to Result
    pub fn parse(&self, input: String) -> Option<Node> {
        let result = self.rules.get(&self.start_rule).unwrap().parse(ParseState {
            input: &input,
            index: 0,
            depth: 0,
            grammar: &self,
            non_terminals: HashSet::new(),
        });
        match result {
            ParseResult::Fail(msg) => {
                println!("Failed to parse due to: {}", msg);
                None
            }
            ParseResult::Success(node, index) => {
                if index == input.len() {
                    Some(node)
                } else {
                    println!("COULD NOT CONSUME FULL INPUT");
                    println!("Consumed {} out of {}", index, input.len());
                    println!("Next char: ({})", input.chars().nth(index)?);
                    None
                }
            }
            ParseResult::EmptySuccess => {
                println!("Empty success...");
                None
            }
        }
    }
}

impl fmt::Display for Grammar {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "=== GRAMMAR ===")?;
        writeln!(f, "START: {}", self.start_rule)?;
        for (name, rule) in &self.rules {
            writeln!(f, "{} -> {}", name, rule)?;
        }
        write!(f, "===")
    }
}
