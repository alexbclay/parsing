use crate::grammar::{Expression, ParseResult, ParseState};
use crate::syntax::{Node, NodeLink, NodeType};
use log::{debug, info};

// Atomic Expressions
#[derive(Clone)]
pub struct AtomicChars {
    pub name: String,
    pub valid_chars: String,
}
impl Expression for AtomicChars {
    fn self_to_string(&self) -> String {
        format!("[{}]", self.valid_chars)
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        match state.input.chars().nth(state.index) {
            None => ParseResult::Fail("Unexpected end of input".to_string()),
            Some(c) => {
                if self.valid_chars.contains(c) {
                    ParseResult::Success(
                        Node {
                            node_type: NodeType::Terminal,
                            name: self.name.clone(),
                            content: NodeLink::Leaf(c),
                        },
                        state.index + 1,
                    )
                } else {
                    ParseResult::Fail(format!("Unexpected character: ({})", c))
                }
            }
        }
    }
}

pub struct AtomicTerminal {
    pub value: char,
}
impl Expression for AtomicTerminal {
    fn self_to_string(&self) -> String {
        format!("'{}'", self.value.escape_debug())
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        match state.input.chars().nth(state.index) {
            None => ParseResult::Fail("Unexpected end of input.".to_string()),
            Some(c) => {
                if c == self.value {
                    ParseResult::Success(
                        Node {
                            node_type: NodeType::Terminal,
                            name: "Char".to_string(),
                            content: NodeLink::Leaf(c),
                        },
                        state.index + 1,
                    )
                } else {
                    ParseResult::Fail(format!("Expected '{}', found ({})", self.value, c))
                }
            }
        }
    }
}
pub struct AtomicAnythingExcept {
    pub not_this: char,
}
impl Expression for AtomicAnythingExcept {
    fn self_to_string(&self) -> String {
        format!("(NOT '{}')", self.not_this.escape_debug())
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        match state.input.chars().nth(state.index) {
            None => ParseResult::Fail("Unexpected end of input.".to_string()),
            Some(c) => {
                if c != self.not_this {
                    ParseResult::Success(
                        Node {
                            node_type: NodeType::Terminal,
                            name: "Char".to_string(),
                            content: NodeLink::Leaf(c),
                        },
                        state.index + 1,
                    )
                } else {
                    ParseResult::Fail(format!("Expected not '{}', found ({})", self.not_this, c))
                }
            }
        }
    }
}

pub struct AtomicNonTerminal {
    pub name: String,
}
impl Expression for AtomicNonTerminal {
    fn self_to_string(&self) -> String {
        self.name.clone()
    }

    fn parse(&self, state: ParseState) -> ParseResult {
        debug!("Parsing: {}", self.name);
        if state.non_terminals.contains(&self.name) {
            return ParseResult::Fail(format!("Already seen non-terminal: {}", self.name));
        }
        let expr = state
            .grammar
            .rules
            .get(&self.name)
            .unwrap_or_else(|| panic!("No expresssion with name '{}'!", self.name));

        let mut new_terminals = state.non_terminals.clone();
        new_terminals.insert(self.name.clone());
        let result = expr.parse(ParseState {
            input: state.input,
            index: state.index,
            depth: state.depth + 1,
            grammar: state.grammar,
            non_terminals: new_terminals,
        });
        match result {
            ParseResult::Fail(msg) => ParseResult::Fail(msg),
            ParseResult::Success(node, new_index) => ParseResult::Success(
                Node {
                    node_type: NodeType::NonTerminal,
                    // steal the contents of the only descendant (unless it is also a non-terminal)
                    name: self.name.clone(),
                    content: match node.node_type {
                        NodeType::NonTerminal => NodeLink::Children(vec![node]),
                        _ => {
                            debug!(
                                ">>>>>>>>>>>>>>>>>>>>>>> {} consuming child: {}",
                                self.name, node.name
                            );
                            node.content
                        }
                    },
                },
                new_index,
            ),
            ParseResult::EmptySuccess => ParseResult::EmptySuccess,
        }
    }
}

pub struct AtomicEmpty;

impl Expression for AtomicEmpty {
    fn self_to_string(&self) -> String {
        "ε".to_string()
    }

    fn parse(&self, state: ParseState) -> ParseResult {
        ParseResult::Success(
            Node {
                node_type: NodeType::Terminal,
                name: "EMPTY".to_string(),
                content: NodeLink::Leaf('ε'),
            },
            state.index,
        )
    }
}
