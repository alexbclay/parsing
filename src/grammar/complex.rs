use crate::grammar::{Expression, ParseResult, ParseState};
use crate::syntax::{Node, NodeLink, NodeType};
use log::{debug, info};
use std::collections::HashSet;

// Complex Expressions
pub struct Sequence {
    pub expressions: Vec<Box<dyn Expression>>,
}
impl Expression for Sequence {
    fn self_to_string(&self) -> String {
        format!(
            "({})",
            self.expressions
                .iter()
                .map(|expr| expr.self_to_string())
                .collect::<Vec<String>>()
                .join(" ")
        )
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        let mut nodes = vec![];
        let mut final_index = state.index;
        let mut non_terminals = state.non_terminals.clone();
        for expression in self.expressions.iter() {
            // check each expression in order.  Fail if any does not match
            let result = expression.parse(ParseState {
                input: state.input,
                index: final_index,
                depth: state.depth + 1,
                grammar: state.grammar,
                non_terminals: non_terminals.clone(),
            });
            match result {
                ParseResult::Fail(msg) => return ParseResult::Fail(msg),
                ParseResult::Success(node, new_index) => {
                    final_index = new_index;
                    nodes.push(node);
                    // success means we can recurse the non-terminals again
                    non_terminals = HashSet::new()
                }
                ParseResult::EmptySuccess => (),
            }
        }
        // All expressions matched!
        ParseResult::Success(
            if nodes.len() == 1 {
                nodes.pop().unwrap()
            } else {
                Node {
                    node_type: NodeType::Other,
                    name: "Sequence".to_string(),
                    content: NodeLink::Children(nodes),
                }
            },
            final_index,
        )
    }
}

pub struct OrderedChoice {
    pub options: Vec<Box<dyn Expression>>,
}

impl Expression for OrderedChoice {
    fn self_to_string(&self) -> String {
        format!(
            "({})",
            self.options
                .iter()
                .map(|expr| expr.self_to_string())
                .collect::<Vec<String>>()
                .join(" | ")
        )
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        let mut fail_msgs = vec![];

        for option in self.options.iter() {
            // check each option in order.  Return the first parse that matches
            debug!("Trying option: {:?}", option);
            let result = option.parse(ParseState {
                input: state.input,
                index: state.index,
                depth: state.depth + 1,
                grammar: state.grammar,
                non_terminals: state.non_terminals.clone(),
            });
            match result {
                ParseResult::Success(node, new_index) => {
                    debug!("--> option success! {:?}", option);
                    return ParseResult::Success(node, new_index);
                }
                ParseResult::Fail(msg) => {
                    debug!("--> option failed: {:?}", option);
                    debug!("--> {}", msg);
                    fail_msgs.push(msg);
                }
                ParseResult::EmptySuccess => {
                    debug!("--> option success (empty) {:?}", option);
                }
            }
        }
        // No option matched
        ParseResult::Fail(format!("No Option matched:\n- {}", fail_msgs.join("\n- ")))
    }
}

pub struct ZeroOrMore {
    pub expression: Box<dyn Expression>,
}
impl Expression for ZeroOrMore {
    fn self_to_string(&self) -> String {
        format!("({})*", self.expression.self_to_string())
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        // loop until something does not match
        let mut children = vec![];
        let mut final_index = state.index;
        loop {
            let result = self.expression.parse(ParseState {
                input: state.input,
                index: final_index,
                depth: state.depth + 1,
                grammar: state.grammar,
                non_terminals: state.non_terminals.clone(),
            });
            match result {
                // no new match, break out of the loop
                ParseResult::Fail(msg) => {
                    debug!("Zero or More: No match. {}", msg);
                    break;
                }
                // found a new match, add it to the list
                ParseResult::Success(node, next_index) => {
                    children.push(node);
                    final_index = next_index;
                }
                // found an empty match, keep going?
                ParseResult::EmptySuccess => (),
            }
        }
        // broke from loop, return what we have (if we have any)
        if children.is_empty() {
            ParseResult::EmptySuccess
        } else {
            ParseResult::Success(
                // combine nodes into a single node
                // these will all have the same name, since it's a repeated expression
                Node {
                    node_type: NodeType::Other,
                    name: children[0].name.clone(),
                    content: NodeLink::Children(children),
                },
                final_index,
            )
        }
    }
}

pub struct OneOrMore {
    pub expression: Box<dyn Expression>,
}
impl Expression for OneOrMore {
    fn self_to_string(&self) -> String {
        format!("{}+", self.expression.self_to_string())
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        let result = self.expression.parse(ParseState {
            input: state.input,
            index: state.index,
            depth: state.depth + 1,
            grammar: state.grammar,
            non_terminals: state.non_terminals.clone(),
        });
        let mut children = vec![];
        let mut final_index = state.index;
        match result {
            // no match for the first one, whole expr does not match
            ParseResult::Fail(msg) => {
                return ParseResult::Fail(msg);
            }
            ParseResult::Success(node, new_index) => {
                children.push(node);
                final_index = new_index;
            }
            ParseResult::EmptySuccess => (),
        }
        // something matches, keep looping until something doesn't match and return the rest
        loop {
            let result = self.expression.parse(ParseState {
                input: state.input,
                index: final_index,
                depth: state.depth + 1,
                grammar: state.grammar,
                non_terminals: state.non_terminals.clone(),
            });
            match result {
                // no new match, break out of the loop
                ParseResult::Fail(msg) => {
                    debug!("One or More: 'More' failed. {}", msg);
                    break;
                }
                // found a new match, add it to the list
                ParseResult::Success(node, next_index) => {
                    children.push(node);
                    final_index = next_index;
                }
                // success without parsing: nothing happens
                // FIXME: this probably results in an infinite loop.  Detect and panic?
                ParseResult::EmptySuccess => (),
            }
        }
        // broke from loop, return what we have
        ParseResult::Success(
            // combine nodes into a single node
            // these will all have the same name, since it's a repeated expression
            Node {
                node_type: NodeType::Other,
                name: children[0].name.clone(),
                content: NodeLink::Children(children),
            },
            // if children.len() == 1 {
            //     children.pop().unwrap()
            // } else {
            //     Node {
            //         node_type: NodeType::Other,
            //         name: "One or More".to_string(),
            //         content: NodeLink::Children(children),
            //     }
            // },
            final_index,
        )
    }
}

pub struct Optional {
    pub expression: Box<dyn Expression>,
}
impl Expression for Optional {
    fn self_to_string(&self) -> String {
        format!("({})?", self.expression.self_to_string())
    }
    fn parse(&self, state: ParseState) -> ParseResult {
        let result = self.expression.parse(ParseState {
            input: state.input,
            index: state.index,
            depth: state.depth + 1,
            grammar: state.grammar,
            non_terminals: state.non_terminals.clone(),
        });
        match result {
            // no match, success!
            ParseResult::Fail(_msg) => ParseResult::EmptySuccess,
            // match, success!
            ParseResult::Success(node, new_index) => ParseResult::Success(node, new_index),
            // match was empty, so is this!
            ParseResult::EmptySuccess => ParseResult::EmptySuccess,
        }
    }
}

pub struct AndPredicate {
    pub expression: Box<dyn Expression>,
}
impl Expression for AndPredicate {
    fn self_to_string(&self) -> String {
        format!("&{}", self.expression.self_to_string())
    }
}

pub struct NotPredicate {
    pub expression: Box<dyn Expression>,
}
impl Expression for NotPredicate {
    fn self_to_string(&self) -> String {
        format!("!{}", self.expression.self_to_string())
    }
}
