use crate::grammar;
use log::{debug, info};
use std::collections::HashMap;
use std::fmt;

#[derive(Debug, Clone)]
pub enum NodeLink {
    Children(Vec<Node>),
    Leaf(char),
}

#[derive(Debug, Clone)]
pub enum NodeType {
    Terminal,
    NonTerminal,
    Other,
}

#[derive(Debug, Clone)]
pub struct Node {
    pub name: String,
    pub node_type: NodeType,
    pub content: NodeLink,
}

impl Node {
    fn nested_fmt(&self, depth: usize, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // depth * ' '
        let mut depth_prefix = "".to_string();
        for _ in 0..depth {
            depth_prefix.push(' ');
        }

        match &self.content {
            NodeLink::Children(kids) => {
                writeln!(f, "{}{} Node: {}", depth_prefix, depth, self.name)?;
                for child in kids.iter() {
                    child.nested_fmt(depth + 1, f)?;
                }
                write!(f, "")
            }
            NodeLink::Leaf(c) => {
                writeln!(f, "{}{} Leaf: {} ({})", depth_prefix, depth, self.name, c)
            }
        }
    }

    // TODO: switch Option<..> to Result<..>?
    pub fn get_expression(&self) -> Option<Box<dyn grammar::Expression>> {
        if let NodeLink::Children(children) = &self.content {
            match self.name.as_str() {
                "sequence-expression" => {
                    if children.len() == 1 {
                        // one child: no need to wrap it in a sequence expression
                        children[0].get_expression()
                    } else {
                        let mut expressions = vec![children[0].get_expression().unwrap()];
                        if let NodeLink::Children(seq) = &children[1].content {
                            for idx in 0..seq.len() {
                                if let NodeLink::Children(contd) = &seq[idx].content {
                                    expressions.push(contd[1].get_expression().unwrap());
                                } else {
                                    panic!("Unexpected Leaf in sequence-expression");
                                }
                            }
                        } else {
                            panic!("Unexpected Leaf in sequence-expression (Seq)")
                        }
                        Some(Box::new(grammar::complex::Sequence { expressions }))
                    }
                }
                "ordered-choice-expression" => {
                    if children.len() == 1 {
                        // one child: no need to wrap it in a choice expression
                        children[0].get_expression()
                    } else {
                        let mut options = vec![children[0].get_expression().unwrap()];
                        // choice is max 2 children, second child is a sequence of sequences
                        if let NodeLink::Children(seq) = &children[1].content {
                            for idx in 0..seq.len() {
                                if let NodeLink::Children(contd) = &seq[idx].content {
                                    options.push(contd[3].get_expression().unwrap());
                                } else {
                                    panic!("Unexpected Leaf in ordered-choice-expression");
                                }
                            }
                        } else {
                            panic!("Unexpected Leaf in ordered-choice-expression (Seq)")
                        }
                        Some(Box::new(grammar::complex::OrderedChoice { options }))
                    }
                }
                "zero-or-more-expression" => {
                    if children.len() == 1 {
                        // not actually a * expr, so pass through
                        children[0].get_expression()
                    } else {
                        // it has the '*'
                        debug!("-- --> ZERO OR MORE");
                        Some(Box::new(grammar::complex::ZeroOrMore {
                            expression: (children[0].get_expression().unwrap()),
                        }))
                    }
                }
                "one-or-more-expression" => {
                    if children.len() == 1 {
                        // not actually a + expr, so pass through
                        children[0].get_expression()
                    } else {
                        // it has the '+'
                        debug!("-- --> ONE OR MORE");
                        Some(Box::new(grammar::complex::OneOrMore {
                            expression: (children[0].get_expression().unwrap()),
                        }))
                    }
                }
                "optional-expression" => {
                    if children.len() == 1 {
                        // not actually a ? expr, so pass through
                        children[0].get_expression()
                    } else {
                        // it has the '*'
                        debug!("-- --> OPTIONAL");
                        Some(Box::new(grammar::complex::Optional {
                            expression: (children[0].get_expression().unwrap()),
                        }))
                    }
                }
                "value-expression" => {
                    if children.len() == 1 {
                        children[0].get_expression()
                    } else {
                        // it is a sub-expression, so skip the '('
                        children[1].get_expression()
                    }
                }
                "atomic-expression" => {
                    // always a single child, so return its expression
                    children[0].get_expression()
                }
                "non-terminal" => {
                    // compress non-terminal's contents (contains 'identifier', which contains a list of leaf nodes)
                    if let NodeLink::Children(leaves) = &children[0].content {
                        Some(Box::new(grammar::atomic::AtomicNonTerminal {
                            name: leaves
                                .iter()
                                .map(|leaf| {
                                    if let NodeLink::Leaf(c) = leaf.content {
                                        c
                                    } else {
                                        panic!(
                                            "Unexpected Non-leaf in identifier {:?}",
                                            &leaf.content
                                        )
                                    }
                                })
                                .collect::<String>(),
                        }))
                    } else {
                        debug!(
                            "get_expr: non-terminal had incorrect contents: {:?}",
                            children
                        );
                        None
                    }
                }
                "terminal" => {
                    // take the middle character
                    if let NodeLink::Leaf(c) = &children[1].content {
                        Some(Box::new(grammar::atomic::AtomicTerminal { value: *c }))
                    } else {
                        debug!("get_expr: Unexpected Non-leaf in terminal: {:?}", children);
                        None
                    }
                }
                "string" => {
                    // return a string
                    if let NodeLink::Children(leaves) = &children[1].content {
                        Some(Box::new(grammar::atomic::AtomicNonTerminal {
                            name: leaves
                                .iter()
                                .map(|leaf| {
                                    if let NodeLink::Leaf(c) = leaf.content {
                                        c
                                    } else {
                                        panic!("Unexpected Non-leaf in string {:?}", &leaf.content)
                                    }
                                })
                                .collect::<String>(),
                        }))
                    } else {
                        debug!("get_expr: string had incorrect contents: {:?}", children);
                        None
                    }
                }
                _ => {
                    // debug!("get_expr: UNIMPLEMENTED: {}", self.name);
                    Some(Box::new(grammar::atomic::AtomicNonTerminal {
                        name: format!("[UNIMPLEMENTED:{}]", self.name),
                    }))
                }
            }
        } else {
            debug!("get_expr: Unexpected Leaf: {:?}", self);
            None
        }
    }

    pub fn get_grammar(parse_tree: Node) -> Option<grammar::Grammar> {
        println!("{}", parse_tree.name);
        // FIXME: make it so that the grammar returns the root node, not the first child
        // if parse_tree.name != "statement" {
        //     panic!("Only take statements!")
        // }
        let mut grammar = grammar::Grammar {
            rules: HashMap::new(),
            start_rule: "START".to_string(),
        };
        if let NodeLink::Children(statements) = parse_tree.content {
            debug!("ROOT NODE: {}", parse_tree.name);
            for statement in statements {
                // each child is a 'statement'
                if statement.name != "statement" {
                    panic!("NOT A STATEMENT!");
                }
                if let NodeLink::Children(children) = statement.content {
                    for child in children {
                        match child.name.as_str() {
                            "comment" | "newline" => {
                                // ignore comments
                                debug!("--> Ignoring comment");
                            }
                            "line" => {
                                debug!("-- --> Grammar line");
                                if let NodeLink::Children(line_parts) = child.content {
                                    let lhs = line_parts[0].get_expression().unwrap();
                                    debug!("-- --> LHS: {:?}", lhs);
                                    let rhs = line_parts[3].get_expression().unwrap();
                                    debug!("-- --> RHS: {:?}", rhs);
                                    grammar.rules.insert(format!("{}", lhs), rhs);
                                }
                                // convert line into a grammar expression
                                // children: non-terminal, ':', ordered-choice-expression
                            }
                            _ => {
                                panic!("!!! UNEXPECTED INPUT: {:?}", child.name);
                            }
                        }
                    }
                } else {
                    panic!("No children for statement! {:?}", statement)
                }
            }
        } else {
            panic!("Root Node should have children")
        }
        Some(grammar)
    }

    fn start_ast_maybe(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        /*
        AST:

        non-terminal:
          non-terminal:
            LEAF
              non-termial
        TODO: make the nodes better while parsing?

        top-down build?
         */
        println!("=== {} ===", self.name);
        match &self.content {
            NodeLink::Children(kids) => {
                if &self.name == "comment" {
                    // ignore comments
                    writeln!(f, "--> COMMENT IGNORED")?;
                } else if &self.name == "zero-or-more-expression"
                    || &self.name == "one-or-more-expression"
                    || &self.name == "optional-expression"
                {
                    if kids.len() == 1 {
                        // no qualifier matched, so we can drop this node
                        kids[0].start_ast_maybe(f)?;
                    } else {
                        // qualifier!
                        writeln!(f, "--> {}", self.name)?;
                        kids[0].start_ast_maybe(f)?;
                        kids[1].start_ast_maybe(f)?;
                    }
                } else if &self.name == "terminal" {
                    // terminal is sequence: [leaf:', leaf:CHAR, leaf']
                    if let NodeLink::Children(node_vec) = &kids[0].content {
                        let terminal = node_vec.get(1).unwrap();
                        if let NodeLink::Leaf(c) = terminal.content {
                            writeln!(f, "--> Terminal: '{}'", c)?;
                        } else {
                            panic!("Unexpected Node in terminal: {:?}", terminal);
                        }
                    }
                } else if &self.name == "identifier" {
                    // identifier is: sequence -> [leaf, leaf, leaf, ...]
                    if let NodeLink::Children(child) = &kids[0].content {
                        writeln!(
                            f,
                            "--> Identifier: \"{}\"",
                            child
                                .iter()
                                .map(|child| if let NodeLink::Leaf(c) = &child.content {
                                    c
                                } else {
                                    panic!("Unexpected Non-leaf in identifier {:?}", &child.content)
                                })
                                .collect::<String>()
                        )?;
                    } else {
                        panic!("Unexpected Leaf in identifier: {:?}", kids[0].content);
                    }
                } else {
                    writeln!(f, "NAME: {} (len: {})", self.name, kids.len())?;
                    for child in kids.iter() {
                        child.start_ast_maybe(f)?;
                    }
                }
                write!(f, "")
            }
            NodeLink::Leaf(c) => {
                if c == &'\n' {
                    writeln!(f, "--> newline")
                } else {
                    writeln!(f, "LEAF: '{}'", c.escape_debug())
                }
            }
        }
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // self.get_grammar();

        self.nested_fmt(0, f)
        // writeln!(f, "REMOVEME PLEASE")
        // let res = self.start_ast_maybe(f);
        // res
    }
}
