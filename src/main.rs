use parsing::grammar;
use parsing::lang;
use std::collections::HashMap;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short = "g", long = "grammar", parse(from_os_str))]
    grammar: std::path::PathBuf,
    #[structopt(short = "p", long = "program", parse(from_os_str))]
    program: std::path::PathBuf,
}

fn main() {
    env_logger::init();
    let args = Opt::from_args();

    // load the files
    let grammar_content = match std::fs::read_to_string(&args.grammar) {
        Err(error) => panic!("{:?}", error),
        Ok(grammar_content) => grammar_content,
    };

    let program = match std::fs::read_to_string(&args.program) {
        Err(error) => panic!("{:?}", error),
        Ok(program_content) => program_content,
    };

    let grammar = match grammar::Grammar::load_from_string(&grammar_content) {
        Err(error) => panic!("{:?}", error),
        Ok(grammar) => grammar,
    };
    // println!("{:?}", grammar);
    // println!("{}", grammar);

    let parse_tree = grammar.parse(program).unwrap();
    // println!("{}", parse_tree);
}
